# PSLauncher

A Minecraft launcher for the ProjectSurvive.cz project.


## Setup

- Install the [Project Lombok](https://projectlombok.org/) IDE plugin
- Clone the [SKCraft Launcher repository](https://github.com/SKCraft/Launcher)
- Open the cloned repository in command line and run
  `./gradlew clean build install` to install the packaged jars to the local
  maven repository
- Clone [JFoenix](https://github.com/jfoenixadmin/JFoenix) into a directory named `JFoenix` (that's what it's called by default)
- Clone this repository beside JFoenix and open it in your preferred IDE
- Use `./gradlew clean pkg` to package the launcher

## SceneBuilder integration

- Get the latest [SceneBuilder](http://gluonhq.com/open-source/scene-builder/) and install it
- Open the binary located at `/opt/SceneBuilder/SceneBuilder`
- Click the settings icon on the header of the `Library` view
- Select `Import JAR/FXML File...`
- Import `./JFoenix/libs/*`, `./JFoenix/subprojects/jfoenix/build/libs/jfoenix-0.0.0-SNAPSHOT.jar` and the latest version of [FontAwesomeFX](https://bitbucket.org/Jerady/fontawesomefx/downloads)
- When done, click at the `Preview` button in the main toolbar, then `Scene Style Sheets` and `Add a Style Sheet...`
- Select `./JFoenix/src/resources/css/*` and confirm.
- Open the FXML you wish to edit.