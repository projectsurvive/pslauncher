package cz.projectsurvive.pslauncher.selfupdate;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.skcraft.concurrency.ObservableFuture;
import com.skcraft.launcher.Launcher;
import com.skcraft.launcher.selfupdate.SelfUpdater;
import com.skcraft.launcher.selfupdate.UpdateChecker;
import cz.projectsurvive.pslauncher.PSLauncher;
import cz.projectsurvive.pslauncher.util.DefaultProgressObservableProvider;
import cz.projectsurvive.pslauncher.util.JFXExecutor;
import cz.projectsurvive.pslauncher.util.JFXUtil;
import cz.projectsurvive.pslauncher.util.ProgressConsumer;
import lombok.Getter;
import lombok.NonNull;
import lombok.extern.java.Log;

import java.io.File;
import java.net.URL;
import java.util.concurrent.CompletableFuture;

/**
 * A copy of the {@link com.skcraft.launcher.update.UpdateManager} customized for JavaFX
 */
@Log
public class PSUpdateManager
{
	private final PSLauncher               psLauncher;
	private final Launcher                 launcher;
	@Getter private final ProgressConsumer consumerCheck;
	@Getter private final ProgressConsumer consumerDownload;

	public PSUpdateManager(ProgressConsumer consumerCheck, ProgressConsumer consumerDownload)
	{
		if(!(consumerCheck instanceof DefaultProgressObservableProvider))
			throw new IllegalArgumentException("The 'consumerCheck' must be an instance of DefaultProgressObservableProvider.");

		psLauncher = PSLauncher.getInstance();
		launcher = psLauncher.getLauncher();
		this.consumerCheck = consumerCheck;
		this.consumerDownload = consumerDownload;
	}

	public CompletableFuture<Boolean> updateIfAvailable()
	{
		ListenableFuture<URL> future = launcher.getExecutor().submit(new UpdateChecker(launcher));
		CompletableFuture<Boolean> resultFuture = new CompletableFuture<>();

		JFXUtil.showProgress(future, ((DefaultProgressObservableProvider) consumerCheck).getDefaultProgressObservable(),
		                     consumerCheck);
		JFXUtil.addErrorDialogCallback(future);

		Futures.addCallback(future, new FutureCallback<URL>()
		{
			@Override
			public void onSuccess(URL result)
			{
				if(result != null)
				{
					performUpdate(result, resultFuture);
					return;
				}

				resultFuture.complete(false);
			}

			@Override
			public void onFailure(Throwable t)
			{
				resultFuture.completeExceptionally(t);
			}
		}, JFXExecutor.getInstance());

		return resultFuture;
	}

	public void performUpdate(@NonNull URL url, CompletableFuture<Boolean> resultFuture)
	{
		SelfUpdater downloader = new SelfUpdater(launcher, url);
		ObservableFuture<File> future = new ObservableFuture<>(launcher.getExecutor().submit(downloader), downloader);

		JFXUtil.showProgress(future, consumerDownload);
		JFXUtil.addErrorDialogCallback(future);

		Futures.addCallback(future, new FutureCallback<File>()
		{
			@Override
			public void onSuccess(File result)
			{
				log.info("An update successfully downloaded.");
				resultFuture.complete(true);
			}

			@Override
			public void onFailure(Throwable t)
			{
				resultFuture.completeExceptionally(t);
			}
		}, JFXExecutor.getInstance());
	}
}
