package cz.projectsurvive.pslauncher.manifest;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.skcraft.launcher.Instance;
import cz.projectsurvive.pslauncher.persistence.PSInstance;
import lombok.Getter;
import lombok.extern.java.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;

@SuppressWarnings("unused")
@Log
@JsonIgnoreProperties(ignoreUnknown = true)
public class PSInstanceManifest
{
    public static final String VERSION_MANIFEST_KEY = "versionManifest";
    public static final String INNER_KEY = "projectSurvive";
    @SuppressWarnings("MismatchedQueryAndUpdateOfCollection")
    private ArrayList<RunTask> postUpdateTasks;
    @JsonIgnore private ObjectNode versionManifest;

    private PSInstanceManifest() {}

    public static PSInstanceManifest load(Instance instance) throws IOException, NullPointerException
    {
        ObjectMapper objectMapper = new ObjectMapper().registerModule(new RunTaskModule());
        ObjectNode versionManifestRoot = (ObjectNode) objectMapper.readTree(instance.getManifestPath()).get(VERSION_MANIFEST_KEY);
        TreeNode root = versionManifestRoot.get(INNER_KEY);

        if(root == null)
            throw new NullPointerException("No extra manifest data found.");

        PSInstanceManifest result = objectMapper.readValue(objectMapper.treeAsTokens(root), PSInstanceManifest.class);
        result.versionManifest = versionManifestRoot;

        return result;
    }

    @JsonIgnore
    public ObjectNode getVersionManifest()
    {
        return versionManifest.deepCopy();
    }

    /**
     * You should really be using {@link PSInstance#getManifestExtra()} instead.
     */
    @Deprecated
    public static Optional<PSInstanceManifest> attemptLoad(Instance instance)
    {
        try
        {
            PSInstanceManifest result = load(instance);

            return Optional.of(result);
        }
        catch (IOException | NullPointerException e)
        {
            log.info("Extra manifest data failed to load: " + e.getLocalizedMessage());
            return Optional.empty();
        }
    }

    public ArrayList<RunTask> getPostUpdateTasks()
    {
        return postUpdateTasks != null ? new ArrayList<>(postUpdateTasks)
                                       : new ArrayList<>();
    }
}
