package cz.projectsurvive.pslauncher.manifest;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;
import java.util.ArrayList;

public class RunTaskModule extends SimpleModule
{
    public RunTaskModule()
    {
        super("RunTaskModule");

        addSerializer(new Serializer());
        addDeserializer(RunTask.class, new Deserializer());
    }

    public class Serializer extends JsonSerializer<RunTask>
    {
        @Override
        public void serialize(RunTask value, JsonGenerator jgen, SerializerProvider provider) throws IOException, JsonProcessingException
        {
            ObjectMapper mapper = new ObjectMapper();
            ObjectNode root = mapper.createObjectNode();
            ArrayNode commandArray = mapper.createArrayNode();

            for(RunTaskCommand command : value.getCommands())
            {
                ArrayNode argumentArray = mapper.createArrayNode();

                command.getArguments().forEach(argumentArray::add);
                commandArray.add(argumentArray);
            }

            root.put("description", value.getDescription());
            root.set("commands", commandArray);

            jgen.writeTree(root);
        }

        @Override
        public Class<RunTask> handledType()
        {
            return RunTask.class;
        }
    }

    public class Deserializer extends JsonDeserializer<RunTask>
    {
        @Override
        public RunTask deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException
        {
            RunTask result = new RunTask();
            ObjectMapper mapper = new ObjectMapper();
            ObjectNode root = mapper.readTree(jp);
            ArrayList<RunTaskCommand> commands = new ArrayList<>();
            ArrayNode commandArray = (ArrayNode) root.get("commands");

            for(JsonNode node : commandArray)
            {
                RunTaskCommand command = new RunTaskCommand();
                ArrayList<String> arguments = new ArrayList<>();
                ArrayNode argumentArray = (ArrayNode) node;

                for(JsonNode argument : argumentArray)
                    arguments.add(argument.asText());

                command.setArguments(arguments);
                commands.add(command);
            }

            result.setDescription(root.get("description").asText());
            result.setCommands(commands);

            return result;
        }

        @Override
        public Class<?> handledType()
        {
            return RunTask.class;
        }
    }
}
