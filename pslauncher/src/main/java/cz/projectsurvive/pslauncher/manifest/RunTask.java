package cz.projectsurvive.pslauncher.manifest;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.java.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Log
@Data
@NoArgsConstructor
public class RunTask
{
    private String description;
    private ArrayList<RunTaskCommand> commands;

    public void execute(ProcessBuilder processBuilder) throws IOException, InterruptedException
    {
        if(commands == null || commands.size() <= 0)
        {
            log.info("No commands specified, skipping task: " + description);
            return;
        }

        log.info("Executing post-update task: " + description);

        for (RunTaskCommand command : commands)
        {
            List<String> arguments = command.getArguments();

            if(arguments.size() <= 0)
            {
                log.info("No arguments specified, skipping one command.");
                continue;
            }

            log.info("Executing post-update command: " + command);

            int exitCode = processBuilder
                    .command(arguments)
                    .inheritIO()
                    .start()
                    .waitFor();

            log.info("Command finished with exit code: " + exitCode);
        }

        log.info("Post-update task finished.");
    }
}
