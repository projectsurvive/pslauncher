package cz.projectsurvive.pslauncher.manifest;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;

@Data
@NoArgsConstructor
public class RunTaskCommand
{
    private ArrayList<String> arguments;

    @Override
    public String toString()
    {
        return arguments.stream().reduce((a, b) -> a + " " + b).get();
    }
}
