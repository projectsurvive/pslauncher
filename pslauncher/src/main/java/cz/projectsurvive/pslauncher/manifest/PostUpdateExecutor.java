package cz.projectsurvive.pslauncher.manifest;

import com.skcraft.concurrency.ObservableFuture;
import com.skcraft.concurrency.ProgressObservable;
import com.skcraft.launcher.Instance;
import com.skcraft.launcher.persistence.Persistence;
import cz.projectsurvive.pslauncher.persistence.PSInstance;
import javafx.scene.control.ProgressBar;
import lombok.EqualsAndHashCode;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.Callable;

@EqualsAndHashCode(callSuper = false)
public class PostUpdateExecutor implements Callable<Instance>, ProgressObservable
{
    private final ObservableFuture<Instance> instanceFuture;
    private double progress;
    private String status;

    public PostUpdateExecutor(ObservableFuture<Instance> instanceFuture)
    {
        this.instanceFuture = instanceFuture;
    }

    @Override
    public Instance call() throws Exception
    {
        Instance instance = instanceFuture.get();
        PSInstance psInstance = PSInstance.get(instance);
        Optional<PSInstanceManifest> manifest = psInstance.getManifestExtra();

        if(!manifest.isPresent())
            return instance;

	    List<RunTask> postUpdateTasks = manifest.get().getPostUpdateTasks();

        if(postUpdateTasks == null || postUpdateTasks.size() <= 0)
            return instance;

	    instance.setInstalled(false);
	    Persistence.commitAndForget(instance);

        int finishedTasks = 0;

        for(RunTask task : postUpdateTasks)
        {
            status = task.getDescription();

            task.execute(
                    new ProcessBuilder()
                    .directory(instance.getContentDir())
            );

            finishedTasks++;
            progress = (double) finishedTasks / postUpdateTasks.size();
        }

	    instance.setInstalled(true);
	    Persistence.commitAndForget(instance);

        return instance;
    }

    @Override
    public double getProgress()
    {
        if(!instanceFuture.isDone())
            return instanceFuture.getProgress();

        return progress == 0 ? ProgressBar.INDETERMINATE_PROGRESS : progress;
    }

    @Override
    public String getStatus()
    {
        if(!instanceFuture.isDone())
            return instanceFuture.getStatus();

        return status;
    }
}
