package cz.projectsurvive.pslauncher.persistence;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.skcraft.launcher.persistence.Persistence;
import com.skcraft.launcher.persistence.Scrambled;
import cz.projectsurvive.pslauncher.PSLauncher;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.File;

/**
 * The persistence API is ingenious! \o/
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Scrambled(value = "The user must not be able to accept the EULA without having it read. That's why we scramble.")
@ToString
public class GlobalData
{
	private static GlobalData instance;
	public static final String FILE_NAME = "global_data.json";
	@Getter @Setter private boolean eulaAccepted = false;
	@Getter @Setter private String rememberedUsername;
    @Getter @Setter private boolean rememberedPasswordUsage;
    @Getter @Setter private String rememberedPassword;

	@JsonIgnore
	public void save()
	{
		Persistence.commitAndForget(this);
	}

	public static File getFile()
	{
		PSLauncher psLauncher = PSLauncher.getInstance();
		File persistenceDir = psLauncher.getPersistenceDir();

		return new File(persistenceDir, FILE_NAME);
	}

	/**
	 * @return Attempts to parse an existing file. Creates a new instance on fail.
	 */
	private static GlobalData load()
	{
		File file = getFile();

        return Persistence.load(file, GlobalData.class);
	}

	public static GlobalData getInstance()
	{
		if(instance != null)
			return instance;

		return instance = load();
	}
}
