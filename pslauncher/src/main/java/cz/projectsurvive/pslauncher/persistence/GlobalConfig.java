package cz.projectsurvive.pslauncher.persistence;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.skcraft.launcher.persistence.Persistence;
import cz.projectsurvive.pslauncher.PSLauncher;
import cz.projectsurvive.pslauncher.controller.LauncherBehavior;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

import java.io.File;

/**
 * The persistence API is ingenious! \o/
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@ToString
public class GlobalConfig
{
    private static GlobalConfig instance;
    public static final String FILE_NAME = "global_config.json";
    @Getter @Setter private boolean displaySnapshotVersions = false;
    @Getter @Setter private boolean rememberUsername = true;
    @Getter @Setter private boolean rememberPassword = true;
    @Getter @Setter @NonNull private LauncherBehavior launcherBehavior = LauncherBehavior.REOPEN;

    @JsonIgnore
    public void save()
    {
        Persistence.commitAndForget(this);
    }

    public static File getFile()
    {
        PSLauncher psLauncher = PSLauncher.getInstance();
        File persistenceDir = psLauncher.getPersistenceDir();

        return new File(persistenceDir, FILE_NAME);
    }

    /**
     * @return Attempts to parse an existing file. Creates a new instance on fail.
     */
    private static GlobalConfig load()
    {
        File file = getFile();

        return Persistence.load(file, GlobalConfig.class);
    }

    public static GlobalConfig getInstance()
    {
        if(instance != null)
            return instance;

        return instance = load();
    }
}

