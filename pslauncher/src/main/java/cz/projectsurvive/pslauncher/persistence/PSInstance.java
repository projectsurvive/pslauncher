package cz.projectsurvive.pslauncher.persistence;

import com.beust.jcommander.internal.Maps;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.skcraft.launcher.Instance;
import com.skcraft.launcher.persistence.Persistence;
import cz.projectsurvive.pslauncher.manifest.PSInstanceManifest;
import lombok.ToString;

import java.io.File;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Pattern;

/**
 * The persistence API is ingenious! \o/
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@ToString
public class PSInstance
{
    public static final String FILE_NAME = "extra_data.json";
    public static final String PREFIX_VANILLA = "Vanilla ";
    public static final String REGEX_VANILLA = "^" + PREFIX_VANILLA + "\\d+\\.\\d+(\\.\\d+)?$";
    public static final Pattern PATTERN_VANILLA = Pattern.compile(REGEX_VANILLA);
    private static final Map<Instance, PSInstance> map = Maps.newHashMap();
    @JsonIgnore private Instance instance;
    @JsonIgnore private Optional<PSInstanceManifest> manifestExtra;

    @JsonIgnore
    public void save()
    {
        Persistence.commitAndForget(this);
    }

    @SuppressWarnings("deprecation")
    @JsonIgnore
    public Optional<PSInstanceManifest> loadManifestExtra()
    {
        return manifestExtra = PSInstanceManifest.attemptLoad(instance);
    }

    @JsonIgnore
    public Optional<PSInstanceManifest> getManifestExtra()
    {
        return manifestExtra != null ? manifestExtra : loadManifestExtra();
    }

    public static File getFile(Instance instance)
    {
        File instanceDir = instance.getDir();

        return new File(instanceDir, FILE_NAME);
    }

    public static PSInstance load(Instance instance)
    {
        File file = getFile(instance);
        PSInstance result = Persistence.load(file, PSInstance.class);
        result.instance = instance;

        map.put(instance, result);

        return result;
    }

    public static PSInstance get(Instance instance)
    {
        PSInstance result = map.get(instance);

        if(result != null)
            return result;

        result = load(instance);

        return result;
    }

    public static boolean isVanilla(Instance instance)
    {
        // TODO: Is there a better way?
        return instance.getName().startsWith(PREFIX_VANILLA);
    }

    public static boolean isSnapshot(Instance instance)
    {
        // TODO: Is there a better way?
        return isVanilla(instance) && !PATTERN_VANILLA.matcher(instance.getName()).matches();
    }
}

