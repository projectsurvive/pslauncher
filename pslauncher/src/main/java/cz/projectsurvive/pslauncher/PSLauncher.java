package cz.projectsurvive.pslauncher;

import com.skcraft.launcher.Configuration;
import com.skcraft.launcher.Launcher;
import cz.projectsurvive.pslauncher.controller.MainWindow;
import cz.projectsurvive.pslauncher.resources.image.ImageHook;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.stage.Stage;
import lombok.Getter;
import lombok.extern.java.Log;

import java.io.File;

@Log
public class PSLauncher extends Application
{
	public static final String DIRECTORY_PERSISTENCE_NAME = "persistence";
    private static String[] launchArguments;
    @Getter private static PSLauncher instance;
    @Getter private Stage primaryStage;
    @Getter private Launcher launcher;

    @Override
    public void start(Stage stage) throws Exception
    {
        Launcher.setupLogger();

        instance = this;
        primaryStage = stage;
        launcher = Launcher.createFromArguments(launchArguments);

	    setConfigDefaults();
        setOnClose();
        setIcons();
        MainWindow.launch(primaryStage);
    }

	private void setConfigDefaults()
	{
		Configuration config = launcher.getConfig();
		int bits = Integer.parseInt(System.getProperty("sun.arch.data.model"));

		if(bits == 32)
		{
			int maxMemory32 = 1024;

			if(config.getMaxMemory() > maxMemory32)
				config.setMaxMemory(maxMemory32);
		}
	}

    private void setOnClose()
    {
        primaryStage.setOnCloseRequest(event -> {
            System.out.println("Close request received.");
            PSLauncher.exit();
        });
    }

    private void setIcons()
    {
        primaryStage.getIcons().setAll(ImageHook.all("icons", true, ImageHook::toJFX));
    }

	public File getPersistenceDir()
	{
		File baseDir = launcher.getBaseDir();
		File result = new File(baseDir, DIRECTORY_PERSISTENCE_NAME);

		if(!result.exists())
			//noinspection ResultOfMethodCallIgnored
			result.mkdirs();

		return result;
	}

    public static void exit()
    {
        Platform.exit();
        System.exit(0);
    }

    public static void main(String[] args) throws ClassNotFoundException
    {
        launchArguments = args;

        launch(PSLauncher.class, launchArguments);
    }
}
