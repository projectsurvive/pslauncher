package cz.projectsurvive.pslauncher.controller;

import com.beust.jcommander.internal.Lists;
import com.google.common.base.Strings;
import com.skcraft.launcher.Configuration;
import com.skcraft.launcher.Launcher;
import com.skcraft.launcher.persistence.Persistence;
import cz.projectsurvive.pslauncher.PSLauncher;
import cz.projectsurvive.pslauncher.persistence.GlobalConfig;
import cz.projectsurvive.pslauncher.resources.layout.LayoutHook;
import cz.projectsurvive.pslauncher.resources.stylesheet.StylesheetHook;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.*;
import lombok.ToString;
import lombok.extern.java.Log;

import java.io.IOException;
import java.util.Optional;

@SuppressWarnings("unused")
@Log
@ToString
public class SettingsDialog extends Alert
{
    public static final ButtonType BUTTON_TYPE_OK = new ButtonType("OK", ButtonBar.ButtonData.OK_DONE);
    public static final ButtonType BUTTON_TYPE_CANCEL = new ButtonType("Zrušit", ButtonBar.ButtonData.CANCEL_CLOSE);

    private final Configuration config;
    private final GlobalConfig psConfig;

    // Visible all the time
    private DialogPane dialogPane;
    @FXML private Button okButton;
    @FXML private Button cancelButton;

    // 'General' pane
    @FXML private CheckBox displaySnapshotVersionsCheckBox;
    @FXML private Label displaySnapshotVersionsLabel;
    @FXML private CheckBox rememberUsernameCheckBox;
    @FXML private Label rememberUsernameLabel;
    @FXML private CheckBox rememberPasswordCheckBox;
    @FXML private Label rememberPasswordLabel;
    @FXML private ChoiceBox<LauncherBehavior> behaviorChoiceBox;
    @FXML private Spinner<Integer> windowWidthSpinner;
    @FXML private Spinner<Integer> windowHeightSpinner;

    // 'Java' pane
    @FXML private CheckBox bundledJvmCheckBox;
    @FXML private Label bundledJvmLabel;
    @FXML private CheckBox autoJvmCheckBox;
    @FXML private Label autoJvmLabel;
    @FXML private Label customJvmLabel;
    @FXML private TextField customJvmTextField;
    @FXML private TextField jvmArgumentsTextField;
    @FXML private Spinner<Integer> minMemorySpinner;
    @FXML private Spinner<Integer> maxMemorySpinner;
    @FXML private Spinner<Integer> permGenSpinner;

    // 'Proxy' pane
    @FXML private CheckBox proxyEnabledCheckBox;
    @FXML private Label proxyEnabledLabel;
    @FXML private TextField proxyHostTextField;
    @FXML private Spinner<Integer> proxyPortSpinner;
    @FXML private TextField proxyUserTextField;
    @FXML private PasswordField proxyPasswordField;

    public SettingsDialog(Configuration config, GlobalConfig psConfig)
    {
        super(AlertType.NONE);

        this.config = config;
        this.psConfig = psConfig;

        try
        {
            construct();
            load();
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }

    private void construct() throws IOException
    {
        FXMLLoader loader = LayoutHook.loader("settings_dialog");
        loader.setController(this);
        dialogPane = loader.load();
        setDialogPane(dialogPane);
        StylesheetHook.applyGlobal(dialogPane);

        initButtons();
        initTabGeneral();
        initTabJava();
        initTabProxy();
    }

    private void initButtons()
    {
        dialogPane.getButtonTypes().addAll(BUTTON_TYPE_OK, BUTTON_TYPE_CANCEL);

        okButton = (Button) dialogPane.lookupButton(BUTTON_TYPE_OK);
        cancelButton = (Button) dialogPane.lookupButton(BUTTON_TYPE_CANCEL);
    }

    private void initTabGeneral()
    {
        // Toggle checkboxes when their labels are pressed
        displaySnapshotVersionsLabel.setOnMouseReleased(event -> displaySnapshotVersionsCheckBox.setSelected(!displaySnapshotVersionsCheckBox.isSelected()));
        rememberUsernameLabel.setOnMouseReleased(event -> rememberUsernameCheckBox.setSelected(!rememberUsernameCheckBox.isSelected()));
        rememberPasswordLabel.setOnMouseReleased(event -> rememberPasswordCheckBox.setSelected(!rememberPasswordCheckBox.isSelected()));

        // Disable remember password, when remember username disabled
        Lists.<Node>newArrayList(rememberPasswordCheckBox, rememberPasswordLabel)
                .forEach(node -> node.disableProperty().bind(rememberUsernameCheckBox.selectedProperty().not()));

        rememberUsernameCheckBox.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if(!newValue)
                rememberPasswordCheckBox.setSelected(false);
        });

        // Add values to the behavior choice box
        behaviorChoiceBox.getItems().addAll(LauncherBehavior.values());

        // Setup spinners
        windowWidthSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, Integer.MAX_VALUE, 0, 100));
        windowHeightSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, Integer.MAX_VALUE, 0, 100));
    }

    private void initTabJava()
    {
        // Toggle checkboxes when their labels are pressed
        bundledJvmLabel.setOnMouseReleased(event -> bundledJvmCheckBox.setSelected(!bundledJvmCheckBox.isSelected()));
        autoJvmLabel.setOnMouseReleased(event -> autoJvmCheckBox.setSelected(!autoJvmCheckBox.isSelected()));

        // Disable custom JVM path input, when not finding automatically
        Lists.<Node>newArrayList(customJvmLabel, customJvmTextField)
                .forEach(node -> node.disableProperty().bindBidirectional(autoJvmCheckBox.selectedProperty()));

        // Setup spinners
        minMemorySpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, Integer.MAX_VALUE, 0, 256));
        maxMemorySpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(256, Integer.MAX_VALUE, 0, 256));
        permGenSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, Integer.MAX_VALUE, 0, 16));
    }

    private void initTabProxy()
    {
        // Toggle checkbox when label is pressed
        proxyEnabledLabel.setOnMouseReleased(event -> proxyEnabledCheckBox.setSelected(!proxyEnabledCheckBox.isSelected()));

        // Disable all other components
        Parent parent = proxyEnabledCheckBox.getParent();

        parent.getChildrenUnmodifiable().stream()
                .filter(node -> node != proxyEnabledCheckBox && node != proxyEnabledLabel)
                .forEach(node -> node.disableProperty().bind(proxyEnabledCheckBox.selectedProperty().not()));

        // Set up spinners
        proxyPortSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, (int) Math.pow(2, 16) - 1));
    }

    // TODO: Add a test for integrity between loading and saving
    public void load()
    {
        log.info("Reading configuration data into the dialog.");

        // 'General' pane
        displaySnapshotVersionsCheckBox.setSelected(psConfig.isDisplaySnapshotVersions());
        rememberUsernameCheckBox.setSelected(psConfig.isRememberUsername());
        rememberPasswordCheckBox.setSelected(psConfig.isRememberPassword());
        behaviorChoiceBox.getSelectionModel().select(psConfig.getLauncherBehavior());
        windowWidthSpinner.getValueFactory().setValue(config.getWindowWidth());
        windowHeightSpinner.getValueFactory().setValue(config.getWidowHeight()); // 'Widow height' -- bhaha.

        // 'Java' pane
        // TODO: bundledJvmCheckBox
        autoJvmCheckBox.setSelected(Strings.isNullOrEmpty(config.getJvmPath()));
        customJvmTextField.setText(config.getJvmPath());
        jvmArgumentsTextField.setText(config.getJvmArgs());
        minMemorySpinner.getValueFactory().setValue(config.getMinMemory());
        maxMemorySpinner.getValueFactory().setValue(config.getMaxMemory());
        permGenSpinner.getValueFactory().setValue(config.getPermGen());

        // 'Proxy' pane
        proxyEnabledCheckBox.setSelected(config.isProxyEnabled());
        proxyHostTextField.setText(config.getProxyHost());
        proxyPortSpinner.getValueFactory().setValue(config.getProxyPort());
        proxyUserTextField.setText(config.getProxyUsername());
        proxyPasswordField.setText(config.getProxyPassword());
    }

    public void save(boolean persist)
    {
        log.info("Writing configuration data from the dialog and saving.");

        // 'General' pane
        psConfig.setDisplaySnapshotVersions(displaySnapshotVersionsCheckBox.isSelected());
        psConfig.setRememberUsername(rememberUsernameCheckBox.isSelected());
        psConfig.setRememberPassword(rememberPasswordCheckBox.isSelected());
        psConfig.setLauncherBehavior(behaviorChoiceBox.getValue());
        config.setWindowWidth(windowWidthSpinner.getValue());
        config.setWidowHeight(windowHeightSpinner.getValue()); // bhahaha, it doesn't get old.

        // 'Java' pane
        // TODO: bundledJvmCheckBox
        config.setJvmPath(autoJvmCheckBox.isSelected() ? null : customJvmTextField.getText());
        config.setJvmArgs(jvmArgumentsTextField.getText());
        config.setMinMemory(minMemorySpinner.getValue());
        config.setMaxMemory(maxMemorySpinner.getValue());
        config.setPermGen(permGenSpinner.getValue());

        // 'Proxy' pane
        config.setProxyEnabled(proxyEnabledCheckBox.isSelected());
        config.setProxyHost(proxyHostTextField.getText());
        config.setProxyPort(proxyPortSpinner.getValue());
        config.setProxyUsername(proxyUserTextField.getText());
        config.setProxyPassword(proxyPasswordField.getText());

        // Save if requested
        if(persist)
            persist();
    }

    public void persist()
    {
        psConfig.save();
        Persistence.commitAndForget(config);
    }

    public static void launch(Configuration config, GlobalConfig psConfig, boolean persist)
    {
        // TODO: probably no need to create a new instance every time
        SettingsDialog settingsDialog = new SettingsDialog(config, psConfig);
        Optional<ButtonType> result = settingsDialog.showAndWait();

        if(result.isPresent() && result.get() == BUTTON_TYPE_OK)
            settingsDialog.save(persist);
    }

    public static void launch()
    {
        PSLauncher psLauncher = PSLauncher.getInstance();
        Launcher launcher = psLauncher.getLauncher();
        Configuration config = launcher.getConfig();
        GlobalConfig psConfig = GlobalConfig.getInstance();

        launch(config, psConfig, true);
    }
}
