/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package cz.projectsurvive.pslauncher.controller;

import com.jfoenix.controls.JFXButton;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIcon;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIconView;
import de.jensd.fx.glyphs.materialicons.MaterialIcon;
import de.jensd.fx.glyphs.materialicons.MaterialIconView;
import javafx.animation.*;
import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.control.Tooltip;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import javafx.util.Duration;

import java.util.ArrayList;
import java.util.List;

/**
 * My customization of {@link com.jfoenix.controls.JFXDecorator}
 * TODO: Consider translating to FXML
 */
public class PSDecorator extends VBox
{
    // Must be set to this value, otherwise the size is glitching on hover
    public static final int GLYPH_SIZE = 14;
    public static final int BORDER_SIZE = 4;
    public static final Color COLOR_BACKGROUND = Color.WHITE;
    public static final Color COLOR_RIPPLE = Color.web("#3F51B5"); // Indigo-500
    public static final Color COLOR_RIPPLE_CLOSE = Color.web("#FF1744");
    private Stage primaryStage;

    private double xOffset = 0;
    private double yOffset = 0;
    private double initMouseX;
    private double initMouseY;
    private double initWidth;
    private double initHeight;
    private double initStageX;
    private double initStageY;

    private boolean allowMove = false;
    private boolean isDragging = false;
    private StackPane contentPlaceHolder = new StackPane();
    private HBox buttonsContainer;
    private ObjectProperty<Runnable> onCloseButtonAction = new SimpleObjectProperty<>(() ->
            this.primaryStage.fireEvent(new WindowEvent(this.primaryStage, WindowEvent.WINDOW_CLOSE_REQUEST)));

    /**
     * Create a window decorator for the specified node with the options:
     * - full screen
     * - maximize
     * - minimize
     *
     * @param stage the primary stage used by the application
     * @param node  the node to be decorated
     */
    public PSDecorator(Stage stage, Node node)
    {
        this(stage, node, true, true);
    }

    /**
     * Create a window decorator for the specified node with the options:
     * - full screen
     * - maximize
     * - minimize
     *
     * @param stage the primary stage used by the application
     * @param node  the node to be decorated
     * @param max   indicates whether to show maximize option or not
     * @param min   indicates whether to show minimize option or not
     */
    public PSDecorator(Stage stage, Node node, boolean max, boolean min)
    {
        super();
        primaryStage = stage;
        primaryStage.initStyle(StageStyle.TRANSPARENT);

        setPickOnBounds(false);
        this.getStyleClass().add("jfx-decorator");

        MaterialDesignIconView minus = new MaterialDesignIconView(MaterialDesignIcon.WINDOW_MINIMIZE);
        minus.getStyleClass().addAll("material-icon-unofficial");
        minus.setGlyphSize(GLYPH_SIZE);
        MaterialDesignIconView resizeMax = new MaterialDesignIconView(MaterialDesignIcon.WINDOW_MAXIMIZE);
        resizeMax.getStyleClass().addAll("material-icon-unofficial");
        resizeMax.setGlyphSize(GLYPH_SIZE);
        MaterialDesignIconView resizeMin = new MaterialDesignIconView(MaterialDesignIcon.WINDOW_RESTORE);
        resizeMin.getStyleClass().addAll("material-icon-unofficial");
        resizeMin.setGlyphSize(GLYPH_SIZE);
        MaterialIconView close = new MaterialIconView(MaterialIcon.CLEAR);
        close.getStyleClass().addAll("material-icon");
        close.setGlyphSize(GLYPH_SIZE);

        JFXButton btnClose = new JFXButton();
        btnClose.getStyleClass().add("jfx-decorator-button");
        btnClose.setCursor(Cursor.HAND);
        btnClose.setOnAction((action) -> onCloseButtonAction.get().run());
        btnClose.setGraphic(close);
        btnClose.setRipplerFill(COLOR_RIPPLE_CLOSE);

        JFXButton btnMin = new JFXButton();
        btnMin.getStyleClass().add("jfx-decorator-button");
        btnMin.setCursor(Cursor.HAND);
        btnMin.setOnAction((action) -> primaryStage.setIconified(true));
        btnMin.setGraphic(minus);
        btnMin.setRipplerFill(COLOR_RIPPLE);

        JFXButton btnMax = new JFXButton();
        btnMax.getStyleClass().add("jfx-decorator-button");
        btnMax.setCursor(Cursor.HAND);
        btnMax.setRipplerFill(COLOR_RIPPLE);
        btnMax.setOnAction((action) ->
        {
            primaryStage.setMaximized(!primaryStage.isMaximized());
            if(primaryStage.isMaximized())
            {
                btnMax.setGraphic(resizeMin);
                btnMax.setTooltip(new Tooltip("Restore Down"));
            }
            else
            {
                btnMax.setGraphic(resizeMax);
                btnMax.setTooltip(new Tooltip("Maximize"));
            }
        });
        btnMax.setGraphic(resizeMax);


        buttonsContainer = new HBox();
        buttonsContainer.getStyleClass().add("jfx-decorator-buttons-container");
        buttonsContainer.setBackground(new Background(new BackgroundFill(COLOR_BACKGROUND, CornerRadii.EMPTY, Insets.EMPTY)));
        // BINDING
//		buttonsContainer.backgroundProperty().bind(Bindings.createObjectBinding(()->{
//			return new Background(new BackgroundFill(decoratorColor.get(), CornerRadii.EMPTY, Insets.EMPTY));
//		}, decoratorColor));


        buttonsContainer.setPadding(new Insets(4));
        buttonsContainer.setAlignment(Pos.CENTER_RIGHT);
        // customize decorator buttons
        List<JFXButton> btns = new ArrayList<>();
        if(min) btns.add(btnMin);
        if(max) btns.add(btnMax);
        btns.add(btnClose);

        buttonsContainer.getChildren().addAll(btns);
        buttonsContainer.addEventHandler(MouseEvent.MOUSE_ENTERED, (enter) -> allowMove = true);
        buttonsContainer.addEventHandler(MouseEvent.MOUSE_EXITED, (enter) ->
        {
            if(!isDragging) allowMove = false;
        });
        buttonsContainer.setMinWidth(180);
        contentPlaceHolder.getStyleClass().add("jfx-decorator-content-container");
        contentPlaceHolder.setMinSize(0, 0);
        contentPlaceHolder.getChildren().add(node);
        ((Region) node).setMinSize(0, 0);
        VBox.setVgrow(contentPlaceHolder, Priority.ALWAYS);
        contentPlaceHolder.getStyleClass().add("resize-border");
//		contentPlaceHolder.setBorder(new Border(new BorderStroke(COLOR_BACKGROUND, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, new BorderWidths(0, 4, 4, 4))));
        // BINDING
//		contentPlaceHolder.borderProperty().bind(Bindings.createObjectBinding(()->{
//			return new Border(new BorderStroke(decoratorColor.get(), BorderStrokeStyle.SOLID, CornerRadii.EMPTY, new BorderWidths(0, 4, 4, 4)));
//		}, decoratorColor));

        Rectangle clip = new Rectangle();
        clip.widthProperty().bind(((Region) node).widthProperty());
        clip.heightProperty().bind(((Region) node).heightProperty());
        node.setClip(clip);
        this.getChildren().addAll(buttonsContainer, contentPlaceHolder);

        // show the drag cursor on the borders
        this.setOnMouseMoved((mouseEvent) ->
        {
            if(!primaryStage.isResizable())
                return;

            double x = mouseEvent.getX();
            double y = mouseEvent.getY();
            Cursor cursor = getCursorForLocation(x, y, false);
            setCursor(cursor);
        });

        this.setOnMousePressed((event) ->
        {
            double x = event.getX();
            double y = event.getY();
            initMouseX = event.getScreenX();
            initMouseY = event.getScreenY();
            initWidth = primaryStage.getWidth();
            initHeight = primaryStage.getHeight();
            initStageX = primaryStage.getX();
            initStageY = primaryStage.getY();
            Cursor cursor = getCursorForLocation(x, y, true);

            if(cursor.equals(Cursor.DEFAULT))
                return;

            // Update the cursor, because the user might be moving the window
            setCursor(cursor);
            isDragging = true;
        });

        // handle drag events on the decorator pane
        this.setOnMouseReleased((mouseEvent) -> isDragging = false);

        this.setOnMouseDragged((mouseEvent) ->
        {
            if(!mouseEvent.isPrimaryButtonDown() || (xOffset == -1 && yOffset == -1))
            {
                return;
            }

            // Long press generates drag event!
            if(primaryStage.isFullScreen() || mouseEvent.isStillSincePress() || primaryStage.isMaximized())
            {
                return;
            }

            double screenX = mouseEvent.getScreenX();
            double screenY = mouseEvent.getScreenY();
            double deltaX = screenX - initMouseX;
            double deltaY = screenY - initMouseY;
            Cursor cursor = this.getCursor();
            boolean consumeEvent = true;

            if(Cursor.E_RESIZE.equals(cursor))
            {
                setStageWidth(initWidth + deltaX);
            }
            else if(Cursor.NE_RESIZE.equals(cursor))
            {
                if(setStageHeight(initHeight - deltaY))
                {
                    primaryStage.setY(initMouseY + deltaY - AnchorPane.getTopAnchor(this));
                }
                setStageWidth(initWidth + deltaX);
            }
            else if(Cursor.SE_RESIZE.equals(cursor))
            {
                setStageWidth(initWidth + deltaX);
                setStageHeight(initHeight + deltaY);
            }
            else if(Cursor.S_RESIZE.equals(cursor))
            {
                setStageHeight(initHeight + deltaY);
            }
            else if(Cursor.W_RESIZE.equals(cursor))
            {
                if(setStageWidth(initWidth - deltaX))
                {
                    primaryStage.setX(initMouseX + deltaX - AnchorPane.getLeftAnchor(this));
                }
            }
            else if(Cursor.SW_RESIZE.equals(cursor))
            {
                if(setStageWidth(initWidth - deltaX))
                {
                    primaryStage.setX(initMouseX + deltaX - AnchorPane.getLeftAnchor(this));
                }
                setStageHeight(initHeight + deltaY);
            }
            else if(Cursor.NW_RESIZE.equals(cursor))
            {
                if(setStageWidth(initWidth - deltaX))
                {
                    primaryStage.setX(initMouseX + deltaX - AnchorPane.getLeftAnchor(this));
                }
                if(setStageHeight(initHeight - deltaY))
                {
                    primaryStage.setY(initMouseY + deltaY - AnchorPane.getTopAnchor(this));
                }
            }
            else if(Cursor.N_RESIZE.equals(cursor))
            {
                if(setStageHeight(initHeight - deltaY))
                {
                    primaryStage.setY(initMouseY + deltaY - AnchorPane.getTopAnchor(this));
                }
            }
            else if(allowMove)
            {
                primaryStage.setX(initStageX + deltaX);
                primaryStage.setY(initStageY + deltaY);
            }
            else
            {
                consumeEvent = false;
            }

            if(consumeEvent)
                mouseEvent.consume();
        });
    }

    private Cursor getCursorForLocation(double x, double y, double borderWidth, boolean pressed)
    {
        if(isRightEdge(x, y))
        {
            if(y < borderWidth)
                return Cursor.NE_RESIZE;
            else if(y > this.getHeight() - borderWidth)
                return Cursor.SE_RESIZE;
            else
                return Cursor.E_RESIZE;
        }
        else if(isLeftEdge(x, y))
        {
            if(y < borderWidth)
                return Cursor.NW_RESIZE;
            else if(y > this.getHeight() - borderWidth)
                return Cursor.SW_RESIZE;
            else
                return Cursor.W_RESIZE;
        }
        else if(isTopEdge(x, y))
            return Cursor.N_RESIZE;
        else if(isBottomEdge(x, y))
            return Cursor.S_RESIZE;
        else if(pressed && allowMove)
            return Cursor.MOVE;
        else
            return Cursor.DEFAULT;
    }

    private Cursor getCursorForLocation(double x, double y, boolean pressed)
    {
        return getCursorForLocation(x, y, BORDER_SIZE, pressed);
    }

    private boolean isRightEdge(double x, double y)
    {
        return x < this.getWidth() && x > this.getWidth() - BORDER_SIZE;
    }

    private boolean isTopEdge(double x, double y)
    {
        return y >= 0 && y < BORDER_SIZE;
    }

    private boolean isBottomEdge(double x, double y)
    {
        return y < this.getHeight() && y > this.getHeight() - BORDER_SIZE;
    }

    private boolean isLeftEdge(double x, double y)
    {
        return x >= 0 && x < BORDER_SIZE;
    }

    boolean setStageWidth(double width)
    {
        boolean limitReached = false;

        if(width < primaryStage.getMinWidth())
        {
            width = primaryStage.getMinWidth();
            limitReached = true;
        }

        if(width >= buttonsContainer.getMinWidth())
        {
            primaryStage.setWidth(width);
            return !limitReached;
        }
        else if(width <= buttonsContainer.getMinWidth())
        {
            width = buttonsContainer.getMinWidth();
            primaryStage.setWidth(width);
        }

        return false;
    }

    boolean setStageHeight(double height)
    {
        boolean limitReached = false;

        if(height < primaryStage.getMinHeight())
        {
            height = primaryStage.getMinHeight();
            limitReached = true;
        }

        if(height >= buttonsContainer.getHeight())
        {
            primaryStage.setHeight(height);
            return !limitReached;
        }
        else if(height <= buttonsContainer.getHeight())
        {
            height = buttonsContainer.getHeight();
            primaryStage.setHeight(height);
        }

        return false;
    }

    /**
     * set a speficed runnable when clicking on the close button
     *
     * @param onCloseButtonAction runnable to be executed
     */
    public void setOnCloseButtonAction(Runnable onCloseButtonAction)
    {
        this.onCloseButtonAction.set(onCloseButtonAction);
    }
}
