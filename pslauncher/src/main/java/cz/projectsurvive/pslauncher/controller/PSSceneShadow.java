package cz.projectsurvive.pslauncher.controller;

import javafx.scene.Node;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class PSSceneShadow extends AnchorPane//VBox
{
    private static final int SHADOW_SIZE_DEFAULT = 19;

    private PSSceneShadow(Node shadowPane, int shadowSize)
    {
        super(shadowPane);

        setStyle(
                "-fx-background-insets: " +
                shadowSize * 3 + "px, " +
                shadowSize * 2 + "px, " +
                shadowSize * 2 + "px, " +
                shadowSize + "px;"
                );
    }

    public static PSSceneShadow construct(Node node, Integer shadowSize_, Stage stage) {
        int shadowSize = shadowSize_ == null ? SHADOW_SIZE_DEFAULT : shadowSize_;
        Node shadowPane = createShadowPane(node, shadowSize);
        PSSceneShadow anchor = new PSSceneShadow(shadowPane, shadowSize);

        setBottomAnchor(shadowPane, (double) shadowSize * 3);
        setLeftAnchor(shadowPane, (double) shadowSize * 2);
        setRightAnchor(shadowPane, (double) shadowSize * 2);
        setTopAnchor(shadowPane, (double) shadowSize);

        if(stage != null)
        {
            stage.setMinWidth(stage.getMinWidth() + shadowSize * 4);
            stage.setMinHeight(stage.getMinHeight() + shadowSize * 4);
            stage.setMaxWidth(stage.getMaxWidth() + shadowSize * 4);
            stage.setMaxHeight(stage.getMaxHeight() + shadowSize * 4);
        }

        return anchor;
    }

    // Create a shadow effect as a halo around the pane and not within
    // the pane's content area.
    static public Node createShadowPane(Node node, int shadowSize) {
        Node shadowPane = node;

        // box-shadow: 0 19px 38px rgba(0,0,0,0.30), 0 15px 12px rgba(0,0,0,0.22);
        // a "real" app would do this in a CSS stylesheet.
        shadowPane.setStyle(
                "-fx-effect: dropshadow(gaussian, rgba(0, 0, 0, 0.30), " + shadowSize * 2 + "px, 0, 0, " + shadowSize + "px);"
                           );

        return shadowPane;
    }
}