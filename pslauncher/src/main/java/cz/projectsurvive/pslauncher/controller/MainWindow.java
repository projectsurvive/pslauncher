package cz.projectsurvive.pslauncher.controller;

import com.beust.jcommander.internal.Lists;
import com.google.common.base.Strings;
import com.google.common.util.concurrent.ListenableFuture;
import com.skcraft.concurrency.ObservableFuture;
import com.skcraft.launcher.Instance;
import com.skcraft.launcher.InstanceList;
import com.skcraft.launcher.Launcher;
import com.skcraft.launcher.auth.AuthenticationException;
import com.skcraft.launcher.auth.Session;
import com.skcraft.launcher.launch.LaunchOptions;
import cz.projectsurvive.pslauncher.PSLauncher;
import cz.projectsurvive.pslauncher.persistence.GlobalConfig;
import cz.projectsurvive.pslauncher.persistence.GlobalData;
import cz.projectsurvive.pslauncher.persistence.PSInstance;
import cz.projectsurvive.pslauncher.resources.image.ImageHook;
import cz.projectsurvive.pslauncher.resources.layout.LayoutHook;
import cz.projectsurvive.pslauncher.resources.stylesheet.StylesheetHook;
import cz.projectsurvive.pslauncher.selfupdate.PSUpdateManager;
import cz.projectsurvive.pslauncher.util.*;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.java.Log;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.concurrent.CancellationException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;

@Log
@RequiredArgsConstructor
@SuppressWarnings("unused")
public class MainWindow implements Initializable
{
    public static final double HEADER_WIDTH = 640;
    public static final double HEADER_HEIGHT = 80;
    @Getter private final Stage stage;
    private PSLauncher psLauncher;
    private Launcher launcher;
    @FXML private ImageView imageViewHeader;
    @FXML private WebView webView;
    @FXML private ProgressBar progressBar;
    @FXML private Node buttonCancel;
    @FXML private Label labelProgress;
    @FXML private Button buttonSettings;
    @FXML private ComboBox<Instance> choiceBoxInstance;
    @FXML private TextField textFieldUsername;
    @FXML private CheckBox checkBoxPassword;
    @FXML private PasswordField passwordFieldPassword;
    @FXML private Button buttonLaunch;

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        psLauncher = PSLauncher.getInstance();
        launcher = psLauncher.getLauncher();

        initImageViewHeader();
        initWebView();
        initProgressBar();
        initButtonSettings();
        initUsernameField();
        initPasswordField();
        initButtonLaunch();

        updateIfAvailable()
                .thenAccept(updated -> {
                    if(updated)
                        UpdateInstalledDialog.showAndClose();

                    JFXExecutor.getInstance().submit(this::loadInstances);
                });
    }

    private void initImageViewHeader()
    {
        Optional<Image> image = ImageHook.random("headers", true, ImageHook::toJFX);

        image.ifPresent(imageViewHeader::setImage);
        imageViewHeader.fitWidthProperty().bind(stage.widthProperty());
        imageViewHeader.fitHeightProperty().bind(stage.widthProperty().multiply(HEADER_HEIGHT / HEADER_WIDTH));
    }

    private void initWebView()
    {
        URL newsURL = launcher.getNewsURL();
        webView.getEngine().load(String.valueOf(newsURL));
    }

    private void initProgressBar()
    {
        // Fix the JFoenix progress bar width
        VBox parent = (VBox) progressBar.getParent();
        progressBar.prefWidthProperty().bind(parent.widthProperty());
    }

    private void initButtonSettings()
    {
        buttonSettings.setOnAction(event -> {
            GlobalConfig globalConfig = GlobalConfig.getInstance();
            boolean displaySnapshotVersions = globalConfig.isDisplaySnapshotVersions();

            SettingsDialog.launch();

            if(displaySnapshotVersions != globalConfig.isDisplaySnapshotVersions())
                processInstanceList(PSLauncher.getInstance().getLauncher().getInstances());
        });
    }

    private void initUsernameField()
    {
        GlobalData globalData = GlobalData.getInstance();
        GlobalConfig globalConfig = GlobalConfig.getInstance();

        if(globalConfig.isRememberUsername())
            textFieldUsername.setText(globalData.getRememberedUsername());

        JFXExecutor.getInstance().execute(textFieldUsername::requestFocus);
    }

    private void initPasswordField()
    {
        GlobalData globalData = GlobalData.getInstance();
        GlobalConfig globalConfig = GlobalConfig.getInstance();

        checkBoxPassword.selectedProperty().addListener((observable, oldValue, newValue) -> {
            boolean newState = newValue != null ? newValue : false;

            passwordFieldPassword.setDisable(!newState);
            passwordFieldPassword.setText(null);

            if(newState)
                passwordFieldPassword.requestFocus();
        });

        checkBoxPassword.setSelected(globalData.isRememberedPasswordUsage());

        if(globalConfig.isRememberPassword())
            passwordFieldPassword.setText(globalData.getRememberedPassword());
    }

    private void initButtonLaunch()
    {
        buttonLaunch.setOnAction(event -> {
            boolean cont = EulaDialog.displayIfNecessary();

            if(cont)
            {
                attemptInstance().ifPresent(instance ->
                        attemptLogin().ifPresent(session ->
                            launchInstance(session, instance)
                        )
                );
            }
        });
    }

    private CompletableFuture<Boolean> updateIfAvailable()
    {
        PSUpdateManager manager = new PSUpdateManager(
                button("Kontroluji nové verze spouštěče.",
                       "Nepodařilo se zkontrolovat nové verze spouštěče.",
                       "Kontrola nových verzí spouštěče byla přerušena."),
                button("Stahuji novou verzi spouštěče.",
                       "Nepodařilo se stáhnout novou verzi spouštěče.",
                       "Stahování nové verze spouštěče bylo přerušeno.")
        );

        return manager.updateIfAvailable();
    }

    private void loadInstances()
    {
        ObservableFuture<InstanceList> future = InstanceUtil.load(button("Načítám herní instance.", "Nastala chyba při načítání herních instancí.",
                "Načítání instancí zrušeno."));

        processInstanceListLater(future, JFXExecutor.getInstance());
    }

    public void processInstanceListLater(ListenableFuture<InstanceList> future, Executor executor)
    {
        future.addListener(() -> {
            try {
                processInstanceList(future.get());
            } catch (InterruptedException | ExecutionException | CancellationException e) {
                throw new RuntimeException(e);
            }
        }, executor);
    }

    public void processInstanceList(InstanceList instanceList)
    {
        List<Instance> instances = Lists.newArrayList(instanceList.getInstances());

        if(instances.size() <= 0)
            return;

        GlobalConfig config = GlobalConfig.getInstance();

        if(!config.isDisplaySnapshotVersions())
            instances.removeIf(PSInstance::isSnapshot);

        choiceBoxInstance.getItems().clear();
        choiceBoxInstance.getItems().addAll(instances);

        SingleSelectionModel<Instance> selectionModel = choiceBoxInstance.getSelectionModel();

        selectionModel.selectedItemProperty().addListener(selected -> {
            instances.stream()
                    .filter(instance -> selected != instance)
                    .forEach(instance -> instance.setSelected(false));
        });

        Optional<Instance> selected = instanceList.getSelected().stream().findAny();

        if(selected.isPresent())
            selectionModel.select(selected.get());
        else
            selectionModel.selectFirst();

        choiceBoxInstance.setDisable(false);
    }

    private Optional<Session> attemptLogin()
    {
        try
        {
            boolean onlineMode = checkBoxPassword.isSelected();
            String username = textFieldUsername.getText();
            String password = onlineMode ? passwordFieldPassword.getText() : null;

            // Validate credentials
            if(onlineMode)
            {
                if(username == null || !(UsernameValidator.validate(username) || EmailValidator.validate(username)))
                    throw new AuthenticationException("Enter a valid username or e-mail.", "Zadejte platné herní jméno nebo e-mail.");
            }
            else
            {
                if(username == null || !UsernameValidator.validate(username))
                    throw new AuthenticationException("Enter a valid username.", "Zadejte platné herní jméno.");
            }

            if(onlineMode && Strings.isNullOrEmpty(password))
                throw new AuthenticationException("Enter a password, or disable it by un-checking it.", "Zadejte heslo, nebo jej vypněte odškrtnutím políčka.");

            // Login
            Session session = SessionUtil.login(username, password);

            // Remeber credentials
            GlobalConfig globalConfig = GlobalConfig.getInstance();
            GlobalData globalData = GlobalData.getInstance();

            globalData.setRememberedUsername(globalConfig.isRememberUsername() ? username : null);
            globalData.setRememberedPasswordUsage(onlineMode);
            globalData.setRememberedPassword(globalConfig.isRememberPassword() ? password : null);
            globalData.save();

            return Optional.of(session);
        }
        catch(InterruptedException | IOException e)
        {
            JFXUtil.showStacktraceDialog("Chyba autentizace", "Nastala chyba při kontaktování autentizačních serverů.", e);
        }
        catch(AuthenticationException e)
        {
            JFXUtil.showErrorDialog(Alert.AlertType.ERROR, "Chyba autentizace", e.getLocalizedMessage(), "Přihlašte se prosím znovu.");
        }

        return Optional.empty();
    }

    private Optional<Instance> attemptInstance()
    {
        Instance instance = choiceBoxInstance.getValue();

        if(instance == null)
        {
            JFXUtil.showErrorDialog(Alert.AlertType.ERROR, "Zvolte instanci", "Nebyla vybrána žádná instance.",
                    "Před kliknutím na tlačítko Spustit zvolte instanci, kterou si přejete spustit.");

            return Optional.empty();
        }

        return Optional.of(instance);
    }

    private void launchInstance(Session session, Instance instance)
    {
        LaunchOptions options = new LaunchOptions.Builder()
                .setInstance(instance)
                .setSession(session)
                .setUpdatePolicy(LaunchOptions.UpdatePolicy.ALWAYS_UPDATE)
                .build();

        InstanceUtil.launch(
                options,
                button("Aktualizuji instanci.", "Nastala chyba při aktualizaci instance.", "Aktualizace zrušena."),
                button("Spouštím Minecraft.", "Nastala chyba při spouštění Minecraftu.", "Spouštění zrušeno."),
                button("Minecraft spuštěn.", "Minecraft byl neočekávaně ukončen.", "Minecraft byl nuceně vypnut.")
        );
    }

    @SuppressWarnings("unchecked")
    private <T extends ProgressConsumer & DefaultProgressObservableProvider> T button(
            String defaultDescription, String defaultFailMessage, String cancelMessage)
    {
        return (T) new ButtonProgressDisplay(progressBar, labelProgress, buttonCancel, defaultDescription, defaultFailMessage, cancelMessage, buttonLaunch);
    }

    @SneakyThrows
    public static MainWindow launch(Stage stage)
    {
        PSLauncher psLauncher = PSLauncher.getInstance();
        Launcher launcher = psLauncher.getLauncher();
        MainWindow controller = new MainWindow(stage);
        FXMLLoader loader = LayoutHook.loader("main_window");

        // Apparently really f**king important.
        loader.setClassLoader(PSLauncher.class.getClassLoader());
        loader.setController(controller);

        Parent root = loader.load();
        PSDecorator decorator = new PSDecorator(stage, root, false, true);

        stage.setWidth(650);
        stage.setMinWidth(650);
        stage.setHeight(400);
        stage.setMinHeight(400);

        PSSceneShadow shadow = PSSceneShadow.construct(decorator, null, stage);
        Scene scene = new Scene(shadow);
        scene.setFill(Color.TRANSPARENT);

        StylesheetHook.applyGlobal(decorator);
        stage.setTitle(launcher.getProperties().getProperty("launcherTitle"));
        stage.setScene(scene);
        stage.show();

        return controller;
    }
}
