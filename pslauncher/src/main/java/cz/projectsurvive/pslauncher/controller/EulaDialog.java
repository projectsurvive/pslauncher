package cz.projectsurvive.pslauncher.controller;

import cz.projectsurvive.pslauncher.persistence.GlobalData;
import cz.projectsurvive.pslauncher.resources.layout.LayoutHook;
import cz.projectsurvive.pslauncher.resources.stylesheet.StylesheetHook;
import javafx.concurrent.Worker;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

import java.io.IOException;
import java.util.Optional;

/**
 * @author Limeth
 */
public class EulaDialog extends Alert
{
	public static final String URL = "https://account.mojang.com/documents/minecraft_eula";
    public static final String RESOURCE_LAYOUT_NAME = "eula_dialog";
        public static final ButtonType BUTTON_TYPE_YES = new ButtonType("Ano", ButtonBar.ButtonData.YES);
        public static final ButtonType BUTTON_TYPE_NO = new ButtonType("Ne", ButtonBar.ButtonData.NO);

	@FXML
	private WebView webView;
	@FXML
	private ProgressBar progressBar;
	private Button  buttonAccept;

	public EulaDialog()
	{
		super(AlertType.CONFIRMATION);

		try
		{
			construct();
		}
		catch(IOException e)
		{
			throw new RuntimeException(e);
		}
	}

	private void construct() throws IOException
	{
        // Load the layout and set this class as it's controller
                FXMLLoader loader = LayoutHook.loader(RESOURCE_LAYOUT_NAME);
                loader.setController(this);
                DialogPane dialogPane = loader.load();
                StylesheetHook.applyGlobal(dialogPane);
        setDialogPane(dialogPane);

        // Add the buttons, since the IDE throws an error when you specify them in the layout
		// TODO: use custom button types to be consistent in language
                dialogPane.getButtonTypes().addAll(BUTTON_TYPE_YES, BUTTON_TYPE_NO);
                buttonAccept = (Button) dialogPane.lookupButton(BUTTON_TYPE_YES);

		WebEngine webEngine = webView.getEngine();
		Worker<Void> worker = webEngine.getLoadWorker();

        // Set up progress bar to display page loading progress
        progressBar.setProgress(ProgressBar.INDETERMINATE_PROGRESS);

        worker.progressProperty().addListener((observable, oldValue, newValue) -> {
            double newValueDouble = newValue.doubleValue();
            double progress;

            if(newValueDouble <= 0)
                progress = ProgressBar.INDETERMINATE_PROGRESS;
            else if(newValueDouble >= 1)
                progress = 0;
            else
                progress = newValueDouble;

            progressBar.setProgress(progress);
        });

        // Enable the accept button only and only if the page is loaded
		worker.stateProperty().addListener((observable, oldValue, newValue) -> {
			boolean disabled = newValue != Worker.State.SUCCEEDED;

			buttonAccept.setDisable(disabled);
		});

        // Finally, start loading the page
		webEngine.load(URL);
	}

	/**
	 * @return True, whether the user should be allowed to continue
	 */
	public static boolean displayIfNecessary()
	{
		GlobalData data = GlobalData.getInstance();

		if(data.isEulaAccepted())
			return true;

		EulaDialog dialog = new EulaDialog();

		Optional<ButtonType> clickedButton = dialog.showAndWait();

                if(!clickedButton.isPresent() || clickedButton.get() != BUTTON_TYPE_YES)
			return false;

		data.setEulaAccepted(true);
		data.save();

		return true;
	}
}
