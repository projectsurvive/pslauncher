package cz.projectsurvive.pslauncher.controller;

import cz.projectsurvive.pslauncher.PSLauncher;
import javafx.application.Platform;
import lombok.*;

@RequiredArgsConstructor
public enum LauncherBehavior
{
    REMAIN("Ponechat spouštěč otevřený"),
    REOPEN("Otevřít spouštěč po ukončení Minecraftu")
    {
        @Override
        public void onStart()
        {
            Platform.runLater(() -> {
                Platform.setImplicitExit(false);
                PSLauncher.getInstance().getPrimaryStage().hide();
            });
        }

        @Override
        public void onEnd()
        {
            Platform.runLater(() -> {
                PSLauncher.getInstance().getPrimaryStage().show();
                Platform.setImplicitExit(true);
            });
        }
    },
    CLOSE("Zavřít spouštěč po spuštění Minecraftu")
    {
        @Override
        public void onStart()
        {
            Platform.runLater(Platform::exit);
        }
    };

    @Getter private final String description;

    public void onStart() {}
    public void onEnd() {}

    @Override
    public String toString()
    {
        return description;
    }
}
