package cz.projectsurvive.pslauncher.controller;

import cz.projectsurvive.pslauncher.PSLauncher;
import cz.projectsurvive.pslauncher.resources.stylesheet.StylesheetHook;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;

public class UpdateInstalledDialog extends Alert
{
    public static final ButtonType BUTTON_CLOSE = new ButtonType("Rozumím", ButtonBar.ButtonData.OK_DONE);

    public UpdateInstalledDialog()
    {
        super(AlertType.INFORMATION, null, BUTTON_CLOSE);

        StylesheetHook.applyGlobal(getDialogPane());
        setTitle("Aktualizace dokončena");
        setHeaderText("Spouštěč byl aktualizován.");
        setContentText("Pro dokončení, zapněte prosím spouštěč znovu.");
    }

    public static void showAndClose()
    {
        new UpdateInstalledDialog().showAndWait();
        PSLauncher.exit();
    }
}
