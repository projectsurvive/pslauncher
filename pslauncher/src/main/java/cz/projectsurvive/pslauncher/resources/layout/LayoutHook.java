package cz.projectsurvive.pslauncher.resources.layout;

import cz.projectsurvive.pslauncher.resources.JarResource;
import javafx.fxml.FXMLLoader;

import java.io.IOException;

public class LayoutHook
{
    public static final String DEFAULT_EXTENSION = "fxml";

    private LayoutHook() {}

    public static FXMLLoader loader(String name, String requiredExtension)
    {
        if(requiredExtension != null && !name.endsWith("." + requiredExtension))
            name += "." + requiredExtension;

        try
        {
            return new FXMLLoader(JarResource.one(LayoutHook.class, name)
                    .orElseThrow(() -> new IOException("Resource not found.")).getURL());
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }

    public static FXMLLoader loader(String name)
    {
        return loader(name, DEFAULT_EXTENSION);
    }
}
