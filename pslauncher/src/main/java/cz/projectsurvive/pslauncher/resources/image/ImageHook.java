package cz.projectsurvive.pslauncher.resources.image;

import com.beust.jcommander.internal.Sets;
import cz.projectsurvive.pslauncher.resources.JarResource;
import javafx.scene.image.Image;

import javax.imageio.ImageIO;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.Optional;
import java.util.Random;
import java.util.Set;
import java.util.function.Function;

public class ImageHook
{
    private static final Random RANDOM = new Random();

    private ImageHook() {}

    /**
     * Load all images from a directory
     *
     * @param dir the directory to load the images from
     * @param loadImage a function used to abstract the image data -- use {@link ImageHook#toAWT(InputStream)} and {@link ImageHook#toJFX(InputStream)}
     * @return a set of the abstracted images
     * @throws RuntimeException as an IOException when an error occurs
     */
    public static <T> Set<T> all(String dir, boolean deep, Function<InputStream, T> loadImage)
    {
        try
        {
            Set<T> result = Sets.newHashSet();
            Set<JarResource> resources = JarResource.all(ImageHook.class, dir, deep);

            for (JarResource resource : resources) {
                InputStream is = resource.getInputStream();
                T image = loadImage.apply(is);

                result.add(image);
            }

            return result;
        }
        catch(IOException e)
        {
            throw new RuntimeException(e);
        }
    }

    /**
     * Load all images from a directory
     *
     * @param name the image name
     * @param loadImage a function used to abstract the image data -- use {@link ImageHook#toAWT(InputStream)} and {@link ImageHook#toJFX(InputStream)}
     * @return a set of the abstracted images
     * @throws RuntimeException as an IOException when an error occurs
     */
    public static <T> Optional<T> one(String name, Function<InputStream, T> loadImage)
    {
        try
        {
            Optional<JarResource> resource = JarResource.one(ImageHook.class, name);

            if(!resource.isPresent())
                return Optional.empty();

            return Optional.of(loadImage.apply(resource.get().getInputStream()));
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }

    /**
     * Load all images from a directory
     *
     * @param dir the directory to load the images from
     * @param loadImage a function used to abstract the image data -- use {@link ImageHook#toAWT(InputStream)} and {@link ImageHook#toJFX(InputStream)}
     * @return a set of the abstracted images
     * @throws RuntimeException as an IOException when an error occurs
     */
    public static <T> Optional<T> random(String dir, boolean deep, Function<InputStream, T> loadImage)
    {
        try
        {
            Set<JarResource> resources = JarResource.all(ImageHook.class, dir, deep);
            int amount = resources.size();

            if(amount <= 0)
                return Optional.empty();

            Iterator<JarResource> resourceIterator = resources.iterator();
            int selectedIndex = RANDOM.nextInt(amount);
            int index = -1;

            while(resourceIterator.hasNext())
            {
                JarResource resource = resourceIterator.next();

                if(++index == selectedIndex)
                {
                    InputStream is = resource.getInputStream();
                    T image = loadImage.apply(is);

                    return Optional.of(image);
                }
            }

            throw new RuntimeException("This exception should be unreachable.");
        }
        catch(IOException e)
        {
            throw new RuntimeException(e);
        }
    }

    public static Image toJFX(InputStream is)
    {
        return new Image(is);
    }

    public static java.awt.Image toAWT(InputStream is)
    {
        try
        {
            return ImageIO.read(is);
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }
}
