package cz.projectsurvive.pslauncher.resources.stylesheet;

import javafx.collections.ObservableList;
import javafx.scene.control.PasswordField;
import resources.css.JFoenixHook;
import cz.projectsurvive.pslauncher.resources.JarResource;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;

import java.io.IOException;
import java.net.URL;
import java.util.List;

public class StylesheetHook
{
    public static final String DEFAULT_EXTENSION = "css";

    private StylesheetHook()
    {
    }

    public static URL url(Class<?> hook, String name, String requiredExtension)
    {
        if(requiredExtension != null && !name.endsWith("." + requiredExtension))
            name += "." + requiredExtension;

        try
        {
            return JarResource.one(hook, name)
                    .orElseThrow(() -> new IOException("Resource not found.")).getURL();
        }
        catch(IOException e)
        {
            throw new RuntimeException(e);
        }
    }

    public static URL url(String name, String requiredExtension)
    {
        return url(StylesheetHook.class, name, requiredExtension);
    }

    public static URL url(Class<?> hook, String name)
    {
        return url(hook, name, DEFAULT_EXTENSION);
    }

    public static URL url(String name)
    {
        return url(name, DEFAULT_EXTENSION);
    }

    public static void applyGlobal(Parent parent)
    {
        applyGlobal(parent.getStylesheets());
    }

    public static void applyGlobal(Scene parent)
    {
        applyGlobal(parent.getStylesheets());
    }

    public static void applyGlobal(ObservableList<String> stylesheets)
    {
        stylesheets.addAll(
                StylesheetHook.url(JFoenixHook.class, "jfoenix-fonts").toString(), //Must be first in order to work
                StylesheetHook.url("global").toString(),
                StylesheetHook.url(JFoenixHook.class, "jfoenix-design").toString()
        );
    }
}
