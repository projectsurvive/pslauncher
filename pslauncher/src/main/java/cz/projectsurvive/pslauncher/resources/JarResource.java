package cz.projectsurvive.pslauncher.resources;

import com.beust.jcommander.internal.Sets;
import lombok.SneakyThrows;
import lombok.Value;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Enumeration;
import java.util.Optional;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.stream.Collectors;

@Value
public class JarResource
{
    private final JarFile jar;
    private final JarEntry entry;

    public InputStream getInputStream() throws IOException
    {
        return jar.getInputStream(entry);
    }

	@SneakyThrows
    public URL getURL()
    {
        try
        {
	        String pathToJar = new File(jar.getName()).toURI().toURL().toExternalForm();
            return new URL("jar:" + pathToJar + "!/" + entry.getName());
        }
        catch (MalformedURLException e)
        {
            throw new RuntimeException(e);
        }
    }

    public static File getLocalJar()
    {
	    String domain = JarResource.class.getProtectionDomain().getCodeSource().getLocation().getPath();

	    try
	    {
		    return new File(URLDecoder.decode(domain, "UTF-8"));
	    }
	    catch(UnsupportedEncodingException e)
	    {
		    throw new InvalidJarPathException("Unable to determine the local jar file path.", e);
	    }
    }

    public static Set<JarResource> any(JarFile jar, String path, boolean deep, boolean exact) throws IOException
    {
        Enumeration<JarEntry> entries = jar.entries();
        Set<JarEntry> result = Sets.newHashSet();

        while(entries.hasMoreElements())
        {
            JarEntry entry = entries.nextElement();
            String currentPath = entry.getName();

            // Exclude directories
            if(currentPath.endsWith("/"))
                continue;

            // If `exact == true`, return a single JarResource, if it exists
            if(exact && currentPath.length() != path.length())
                continue;

            // If not deep, ignore paths with more slashes
            if(!deep && currentPath.indexOf('/', path.length()) == -1)
                continue;

            if(!currentPath.startsWith(path))
                continue;

            result.add(entry);
        }

        return result.stream().map(entry -> new JarResource(jar, entry)).collect(Collectors.toSet());
    }

    public static Set<JarResource> any(File file, String path, boolean deep, boolean exact) throws IOException
    {
        return any(new JarFile(file), path, deep, exact);
    }

    public static Set<JarResource> any(String path, boolean deep, boolean exact) throws IOException
    {
        return any(getLocalJar(), path, deep, exact);
    }

    public static Set<JarResource> any(Class<?> hook, String path, boolean deep, boolean exact) throws IOException
    {
        path = hook.getPackage().getName().replace('.', '/') + "/" + path;

        return any(path, deep, exact);
    }

    private static String directorify(String path)
    {
        return path.endsWith("/") ? path : path + "/";
    }

    public static Set<JarResource> all(JarFile jar, String directory, boolean deep) throws IOException
    {
        return any(jar, directorify(directory), deep, false);
    }

    public static Set<JarResource> all(File file, String directory, boolean deep) throws IOException
    {
        return any(file, directorify(directory), deep, false);
    }

    public static Set<JarResource> all(String directory, boolean deep) throws IOException
    {
        return any(directorify(directory), deep, false);
    }

    public static Set<JarResource> all(Class<?> hook, String directory, boolean deep) throws IOException
    {
        return any(hook, directorify(directory), deep, false);
    }

    public static Optional<JarResource> one(JarFile jar, String path) throws IOException
    {
        return any(jar, path, true, true).stream().reduce(JarResource::requireOne);
    }

    public static Optional<JarResource> one(File file, String path) throws IOException
    {
        return any(file, path, true, true).stream().reduce(JarResource::requireOne);
    }

    public static Optional<JarResource> one(String path) throws IOException
    {
        return any(path, true, true).stream().reduce(JarResource::requireOne);
    }

    public static Optional<JarResource> one(Class<?> hook, String path) throws IOException
    {
        return any(hook, path, true, true).stream().reduce(JarResource::requireOne);
    }

    private static JarResource requireOne(JarResource a,  JarResource b)
    {
        throw new DuplicateJarResourceException();
    }

    public static class DuplicateJarResourceException extends RuntimeException
    {
        private DuplicateJarResourceException()
        {
            super("More jar entries found.");
        }
    }

	public static class InvalidJarPathException extends RuntimeException
	{
		public InvalidJarPathException(String s, Throwable t)
		{
			super(s, t);
		}
	}
}
