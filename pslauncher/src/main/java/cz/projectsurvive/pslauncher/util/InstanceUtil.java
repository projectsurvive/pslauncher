package cz.projectsurvive.pslauncher.util;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.MoreExecutors;
import com.skcraft.concurrency.ObservableFuture;
import com.skcraft.launcher.Instance;
import com.skcraft.launcher.InstanceList;
import com.skcraft.launcher.Launcher;
import com.skcraft.launcher.auth.Session;
import com.skcraft.launcher.launch.LaunchListener;
import com.skcraft.launcher.launch.LaunchOptions;
import com.skcraft.launcher.persistence.Persistence;
import com.skcraft.launcher.update.Updater;
import cz.projectsurvive.pslauncher.PSLauncher;
import cz.projectsurvive.pslauncher.manifest.PostUpdateExecutor;
import cz.projectsurvive.pslauncher.persistence.GlobalConfig;
import javafx.scene.control.Alert;
import lombok.NonNull;
import lombok.extern.java.Log;
import org.apache.commons.io.FileUtils;

import javax.annotation.Nonnull;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.logging.Level;

@Log
public final class InstanceUtil
{
    private InstanceUtil() {}

    public static ObservableFuture<InstanceList> load(ProgressConsumer consumer)
    {
        PSLauncher psLauncher = PSLauncher.getInstance();
        Launcher launcher = psLauncher.getLauncher();
        InstanceList.Enumerator loader = launcher.getInstances().createEnumerator();
        ObservableFuture<InstanceList> future = new ObservableFuture<>(launcher.getExecutor().submit(loader), loader);

        JFXUtil.showProgress(future, loader, consumer);
        JFXUtil.addErrorDialogCallback(future);

        return future;
    }

    /**
     * Own version of {@link com.skcraft.launcher.launch.LaunchSupervisor#launch(LaunchOptions)}
     * to enable JavaFX functionality
     */
    @SuppressWarnings("Duplicates")
    public static <T extends ProgressConsumer & DefaultProgressObservableProvider> void launch(
            LaunchOptions options, ProgressConsumer updatingConsumer, ProgressConsumer launchingConsumer, T processConsumer)
    {
        PSLauncher psLauncher = PSLauncher.getInstance();
        Launcher launcher = psLauncher.getLauncher();
        Instance instance = options.getInstance();
        LaunchListener listener = options.getListener();
        @NonNull Session session = options.getSession();

        try
        {
            boolean update = options.getUpdatePolicy().isUpdateEnabled() && instance.isUpdatePending();

            // Store last access date
            Date now = new Date();
            instance.setLastAccessed(now);
            Persistence.commitAndForget(instance);

            // If we have to update, we have to update
            if(!instance.isInstalled())
                update = true;

            if(update)
            {
                // Execute the updater
                Updater updater = new Updater(launcher, instance);
                updater.setOnline(options.getUpdatePolicy() == LaunchOptions.UpdatePolicy.ALWAYS_UPDATE || session.isOnline());
                ObservableFuture<Instance> updatedFuture = new ObservableFuture<>(
                        launcher.getExecutor().submit(updater), updater);

                PostUpdateExecutor postUpdateExecutor = new PostUpdateExecutor(updatedFuture);
                ObservableFuture<Instance> future = new ObservableFuture<>(
                        launcher.getExecutor().submit(postUpdateExecutor), postUpdateExecutor
                );

                // Show progress
                JFXUtil.showProgress(future, updatingConsumer);
                JFXUtil.addErrorDialogCallback(future);

                // Update the list of instances after updating
                future.addListener(() -> JFXExecutor.getInstance().execute(listener::instancesUpdated), JFXExecutor.getInstance());

                // On success, launch also
                Futures.addCallback(future, new FutureCallback<Instance>() {
                    @Override
                    public void onSuccess(Instance result)
                    {
                        launch(instance, session, listener, launchingConsumer, processConsumer);
                    }

                    @Override
                    public void onFailure(@Nonnull Throwable t) {}
                }, JFXExecutor.getInstance());
            }
            else
            {
                launch(instance, session, listener, launchingConsumer, processConsumer);
            }
        }
        catch (ArrayIndexOutOfBoundsException e)
        {
            JFXUtil.showErrorDialog(Alert.AlertType.ERROR, "No instance", "Nebyla vybrána žádná herní instance.", "Vyberte herní instanci a spusťte Minecraft znovu.");
        }
    }

    /**
     * Own version of {@link com.skcraft.launcher.launch.LaunchSupervisor#launch(Window, Instance, Session, LaunchListener)}
     * to enable JavaFX functionality
     */
    @SuppressWarnings("Duplicates")
    public static <T extends ProgressConsumer & DefaultProgressObservableProvider> void launch(
            Instance instance, Session session, final LaunchListener listener, ProgressConsumer launchingConsumer, T processConsumer)
    {
	    PSLauncher psLauncher = PSLauncher.getInstance();
	    Launcher launcher = psLauncher.getLauncher();
	    final File extractDir = launcher.createExtractDir();

	    // Get the process
	    PSRunner launchingTask = new PSRunner(launcher, instance, session, extractDir);
	    ObservableFuture<Process> launchingFuture = new ObservableFuture<>(launcher.getExecutor().submit(launchingTask), launchingTask);

	    // Show process for the process retrieval
	    JFXUtil.showProgress(launchingFuture, launchingConsumer);
	    JFXUtil.addErrorDialogCallback(launchingFuture);

	    // If the process is started, get rid of this window
	    Futures.addCallback(launchingFuture, new FutureCallback<Process>()
	    {
		    @Override
		    public void onSuccess(Process result)
		    {
			    JFXExecutor.getInstance().execute(listener::gameStarted);
                GlobalConfig.getInstance().getLauncherBehavior().onStart();
			    monitor(extractDir, result, listener, processConsumer);
		    }

		    @Override
		    public void onFailure(@Nonnull Throwable t)
		    {
		    }
	    });
    }

	public static <T extends ProgressConsumer & DefaultProgressObservableProvider> void monitor(
			File extractDir, Process process, LaunchListener listener, T processConsumer)
	{
		PSLauncher psLauncher = PSLauncher.getInstance();
		Launcher launcher = psLauncher.getLauncher();

        // Watch the created process
//        ListenableFuture<?> processFuture = Futures.transform(
//                launchingFuture, new LaunchProcessHandler(launcher), launcher.getExecutor());
//        JFXUtil.addErrorDialogCallback(processFuture);

        ListenableFuture<?> processFuture = launcher.getExecutor().submit(new ProcessTask(process));

        JFXUtil.showProgress(processFuture, processConsumer.getDefaultProgressObservable(), processConsumer);
        JFXUtil.addErrorDialogCallback(processFuture);

        // Clean up at the very end
        processFuture.addListener(() -> {
            try
            {
                FileUtils.deleteDirectory(extractDir);
            }
            catch (IOException e)
            {
                log.log(Level.WARNING, "Failed to clean up " + extractDir.getAbsolutePath(), e);
            }

            JFXExecutor.getInstance().execute(listener::gameClosed);
            GlobalConfig.getInstance().getLauncherBehavior().onEnd();
        }, MoreExecutors.sameThreadExecutor());
    }
}
