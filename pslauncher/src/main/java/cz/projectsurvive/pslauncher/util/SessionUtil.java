package cz.projectsurvive.pslauncher.util;

import com.skcraft.launcher.Launcher;
import com.skcraft.launcher.auth.AuthenticationException;
import com.skcraft.launcher.auth.LoginService;
import com.skcraft.launcher.auth.OfflineSession;
import com.skcraft.launcher.auth.Session;
import cz.projectsurvive.pslauncher.PSLauncher;
import lombok.NonNull;

import java.io.IOException;
import java.util.List;

public final class SessionUtil
{
    private SessionUtil() {}

    public static Session login(@NonNull String username, String password) throws InterruptedException, IOException, AuthenticationException {
        if(password == null)
            return loginOffline(username);

        if(password.length() <= 0)
            throw new IllegalArgumentException("The password length must be greater than 0, or the password must be `null`.");

        return loginOnline(username, password);
    }

    public static OfflineSession loginOffline(@NonNull String username)
    {
        return new OfflineSession(username);
    }

    public static Session loginOnline(@NonNull String username, @NonNull String password) throws InterruptedException, IOException, AuthenticationException
    {
        PSLauncher psLauncher = PSLauncher.getInstance();
        Launcher launcher = psLauncher.getLauncher();
        LoginService service = launcher.getLoginService();
        List<? extends Session> identities = service.login(launcher.getProperties().getProperty("agentName"), username, password);

        // The list of identities (profiles in Mojang terms) corresponds to whether the account
        // owns the game, so we need to check that
        if (identities.size() > 0)
        {
//            // Set offline enabled flag to true
//            Configuration config = launcher.getConfig();
//
//            if (!config.isOfflineEnabled())
//            {
//                config.setOfflineEnabled(true);
//                Persistence.commitAndForget(config);
//            }
//
//            Persistence.commitAndForget(getAccounts());

            return identities.get(0);
        }
        else
        {
            throw new AuthenticationException("Minecraft not owned", "Nevlastníte Minecraft. Přihlašte se bez hesla.");
        }
    }
}
