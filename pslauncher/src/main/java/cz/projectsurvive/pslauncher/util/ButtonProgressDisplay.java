package cz.projectsurvive.pslauncher.util;

import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class ButtonProgressDisplay extends CancellableProgressDisplay
{
    private final Set<Node> buttons;

    public ButtonProgressDisplay(ProgressBar progressBar, Label label, Node cancelButton, String defaultDescription, String defaultFailMessage, String cancelMessage, Node... buttons)
    {
        super(progressBar, label, cancelButton, defaultDescription, defaultFailMessage, cancelMessage);

        this.buttons = Arrays.stream(buttons).filter(button -> button != null).collect(Collectors.toSet());
    }

    @Override
    public void onStart()
    {
        super.onStart();
        buttonsSetDisable(true);
    }

    @Override
    public void onEnd()
    {
        super.onEnd();
        buttonsSetDisable(false);
    }

    protected void buttonsSetDisable(boolean disable)
    {
        buttons.stream().map(Node::disableProperty).forEach(disableProperty -> disableProperty.set(disable));
    }

    public void setButtons(Node... buttons)
    {
        this.buttons.clear();
        this.buttons.addAll(Arrays.stream(buttons).filter(button -> button != null).collect(Collectors.toSet()));
    }

    public Set<Node> getButtons()
    {
        return new HashSet<>(buttons);
    }
}
