package cz.projectsurvive.pslauncher.util;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.skcraft.concurrency.ObservableFuture;
import com.skcraft.concurrency.ProgressObservable;
import com.skcraft.launcher.LauncherException;
import com.skcraft.launcher.util.SharedLocale;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.Value;
import lombok.extern.java.Log;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Optional;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CancellationException;
import java.util.logging.Level;

@Log
public final class JFXUtil
{
    private JFXUtil() {}

    public static void showStacktraceDialog(String header, String content, Throwable ex)
    {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Chybové hlášení");
        alert.setHeaderText(header);
        alert.setContentText(content);

        // Create expandable Exception.
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        ex.printStackTrace(pw);
        String exceptionText = sw.toString();

        Label label = new Label("Detaily chyby (prosím nahlašte autorům):");

        TextArea textArea = new TextArea(exceptionText);
        textArea.setEditable(false);
        textArea.setWrapText(true);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(label, 0, 0);
        expContent.add(textArea, 0, 1);

        // Set expandable Exception into the dialog pane.
        alert.getDialogPane().setExpandableContent(expContent);

        alert.show();
    }

    public static void showErrorDialog(Alert.AlertType type, String title, String header, String content)
    {
        Alert alert = new Alert(type);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(content);

        alert.show();
    }

    public static void addErrorDialogCallback(ListenableFuture<?> future)
    {
        Futures.addCallback(future, new FutureCallback<Object>()
        {
            @Override
            public void onSuccess(@Nullable Object result) {}

            @Override
            public void onFailure(@Nonnull Throwable t)
            {
                if (t instanceof InterruptedException || t instanceof CancellationException)
                    return;

                String message;
                if (t instanceof LauncherException)
                {
                    message = t.getLocalizedMessage();
                    t = t.getCause();
                }
                else
                {
                    message = t.getLocalizedMessage();
                    if (message == null)
                        message = SharedLocale.tr("errors.genericError");
                }
                log.log(Level.WARNING, "Task failed", t);
                showStacktraceDialog(message, SharedLocale.tr("errorTitle"), t);
            }
        }, JFXExecutor.getInstance());
    }

    public static void showProgress(final ListenableFuture<?> future, ProgressObservable observable, ProgressConsumer consumer)
    {
        if(consumer instanceof CancellationHandler)
            ((CancellationHandler) consumer).setOnCancelled(() -> future.cancel(true));

        final Timer timer = new Timer();
        final UpdateProgress updateProgress = new UpdateProgress(consumer, observable);

        JFXExecutor.getInstance().execute(consumer::onStart);
        timer.scheduleAtFixedRate(updateProgress, 400, 400);
        Futures.addCallback(future, new FutureCallback<Object>() {
            @Override
            public void onSuccess(@Nullable Object result) {
                timer.cancel();
                JFXExecutor.getInstance().execute(consumer::onFinish);
            }

            @Override
            public void onFailure(@Nonnull Throwable throwable) {
                timer.cancel();

                Runnable reaction;

                if(throwable instanceof CancellationException && consumer instanceof CancellationHandler)
                    reaction = ((CancellationHandler) consumer)::onCancel;
                else
                    reaction = () -> consumer.onFail(Optional.ofNullable(throwable.getLocalizedMessage()));

                JFXExecutor.getInstance().execute(reaction);
            }
        }, JFXExecutor.getInstance());
    }

    public static void showProgress(ObservableFuture<?> observableFuture, ProgressConsumer consumer)
    {
        showProgress(observableFuture, observableFuture, consumer);
    }

    @Value
    @EqualsAndHashCode(callSuper = false)
    private static class UpdateProgress extends TimerTask {
        @NonNull private final ProgressConsumer consumer;
        @NonNull private final ProgressObservable observable;

        @Override
        public void run() {
            JFXExecutor.getInstance().execute(() -> {
                double progress = observable.getProgress();
                String status = observable.getStatus();

                if(status != null)
                {
                    int index = status.indexOf('\n');

                    if(index != -1)
                    {
                        status = status.substring(0, index);
                    }
                }

                consumer.onProgressChange(progress, Optional.ofNullable(status));
            });
        }
    }
}
