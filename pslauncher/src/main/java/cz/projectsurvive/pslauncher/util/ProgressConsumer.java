package cz.projectsurvive.pslauncher.util;

import java.util.Optional;

public interface ProgressConsumer
{
    void onProgressChange(double progress, Optional<String> description);
    void onStart();
    void onEnd();
    default void onFinish() { onEnd(); }
    default void onFail(Optional<String> description) { onEnd(); }
}
