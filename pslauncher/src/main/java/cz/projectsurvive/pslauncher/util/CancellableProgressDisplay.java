package cz.projectsurvive.pslauncher.util;

import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBase;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.input.InputEvent;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NonNull;

public class CancellableProgressDisplay extends ProgressDisplay implements CancellationHandler
{
    @Getter @NonNull private final Node cancelButton;
    @Getter private final String cancelMessage;
    @Getter private Runnable onCancelled;
    @Getter(AccessLevel.PROTECTED) private EventHandler<ActionEvent> handler;

    public CancellableProgressDisplay(ProgressBar progressBar, Label label, Node cancelButton, String defaultDescription, String defaultFailMessage, String cancelMessage)
    {
        super(progressBar, label, defaultDescription, defaultFailMessage);

        this.cancelButton = cancelButton;
        this.cancelMessage = cancelMessage;
    }

    @Override
    public void onCancel()
    {
        onEnd();
        getLabel().setText(cancelMessage);
    }

    @Override
    public void setOnCancelled(Runnable onCancelled)
    {
        if(!isCancellable())
            return;

        this.onCancelled = onCancelled;

        if(this.onCancelled == null)
        {
            disableCancelButton();
            return;
        }

        setActionHandler(onCancelled::run);
        cancelButton.setDisable(false);
    }

    protected void disableCancelButton()
    {
        if(getActionHandler() == handler)
        {
            setActionHandler(null);
            cancelButton.setDisable(true);
        }

        onCancelled = null;
        handler = null;
    }

    private EventHandler<?> getActionHandler()
    {
        if(cancelButton instanceof ButtonBase)
            return ((ButtonBase) cancelButton).getOnAction();
        else
            return cancelButton.getOnMouseReleased();
    }

    private void setActionHandler(Runnable runnable)
    {
        if(cancelButton instanceof ButtonBase)
            ((ButtonBase) cancelButton).setOnAction(runnable == null ? null : event -> runnable.run());
        else
            cancelButton.setOnMouseReleased(runnable == null ? null : event -> runnable.run());
    }

    @Override
    public void onEnd()
    {
        super.onEnd();

        if(isCancellable())
            disableCancelButton();
    }

    public boolean isCancellable()
    {
        return cancelButton != null;
    }
}
