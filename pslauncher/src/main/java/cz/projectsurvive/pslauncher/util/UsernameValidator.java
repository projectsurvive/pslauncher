package cz.projectsurvive.pslauncher.util;

import java.util.regex.Pattern;

public class UsernameValidator
{
    public static final String USERNAME_REGEX = "^[a-zA-Z0-9_]{3,16}$";
    public static final Pattern USERNAME_PATTERN = Pattern.compile(USERNAME_REGEX);

    private UsernameValidator() {}

    /**
     * Validate hex with regular expression
     *
     * @param hex
     *            hex for validation
     * @return true valid hex, false invalid hex
     */
    public static boolean validate(final String hex)
    {
        return USERNAME_PATTERN.matcher(hex).matches();
    }
}
