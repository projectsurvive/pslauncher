package cz.projectsurvive.pslauncher.util;

import com.skcraft.concurrency.ProgressObservable;

public interface DefaultProgressObservableProvider
{
    ProgressObservable getDefaultProgressObservable();
}
