package cz.projectsurvive.pslauncher.util;

import com.google.common.collect.Lists;
import com.google.common.util.concurrent.AbstractListeningExecutorService;
import javafx.application.Platform;
import lombok.Getter;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class JFXExecutor extends AbstractListeningExecutorService
{
    @Getter private static final JFXExecutor instance = new JFXExecutor();

    private JFXExecutor() {}

    @Override
    public void shutdown() {}

    @Nonnull
    @Override
    public List<Runnable> shutdownNow()
    {
        return Lists.newArrayList();
    }

    @Override
    public boolean isShutdown()
    {
        return false;
    }

    @Override
    public boolean isTerminated()
    {
        return false;
    }

    @Override
    public boolean awaitTermination(long timeout, @Nonnull TimeUnit unit) throws InterruptedException
    {
        return false;
    }

    @Override
    public void execute(@Nonnull Runnable command)
    {
        Platform.runLater(command);
    }
}
