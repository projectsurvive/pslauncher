package cz.projectsurvive.pslauncher.util;

public interface CancellationHandler
{
    /**
     * Called by the onCancelled parameter of {@link #setOnCancelled(Runnable)} method.
     */
    void onCancel();
    void setOnCancelled(Runnable onCancelled);
}
