package cz.projectsurvive.pslauncher.util;

import com.skcraft.concurrency.ProgressObservable;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;

import java.util.Optional;

@Log
@EqualsAndHashCode
@RequiredArgsConstructor
public class ProgressDisplay implements ProgressConsumer, DefaultProgressObservableProvider
{
    @Getter @NonNull private final ProgressBar progressBar;
    @Getter @NonNull private final Label label;
    @Getter @NonNull private final String defaultDescription;
    @Getter @NonNull private final String defaultFailMessage;

    /**
     * Called when one of the following functions is called:
     * - {@link #onFinish()}
     * - {@link #onFail(Optional)}
     */
    public void onEnd()
    {
        progressBar.setDisable(true);
        progressBar.setProgress(0);
    }

    @Override
    public void onStart()
    {
        progressBar.setDisable(false);
    }

    @Override
    public void onProgressChange(double progress, Optional<String> description)
    {
        progressBar.setProgress(progress < 1 ? progress : ProgressBar.INDETERMINATE_PROGRESS);
        label.setText(description.orElseGet(this::getDefaultDescription));
    }

    @Override
    public void onFinish()
    {
        onEnd();
        label.setText(null);
    }

    @Override
    public void onFail(Optional<String> description)
    {
        onEnd();
        label.setText(description.orElse(defaultFailMessage));
    }

    @Override
    public ProgressObservable getDefaultProgressObservable()
    {
        return new DefaultProgressObservable();
    }

    public class DefaultProgressObservable implements ProgressObservable
    {
        private DefaultProgressObservable() {}

        @Override
        public double getProgress()
        {
            return ProgressBar.INDETERMINATE_PROGRESS;
        }

        @Override
        public String getStatus()
        {
            return getDefaultDescription();
        }
    }
}
