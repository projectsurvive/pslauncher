package cz.projectsurvive.pslauncher.util;

import javafx.concurrent.Task;
import lombok.EqualsAndHashCode;
import lombok.Value;

@Value
@EqualsAndHashCode(callSuper = false)
public class ProcessTask extends Task<Integer>
{
    private final Process process;

    @Override
    public Integer call() throws Exception
    {
        try
        {
            while(process.isAlive())
                Thread.sleep(100);

            return process.exitValue();
        }
        catch(InterruptedException e)
        {
            process.destroyForcibly();
            Thread.currentThread().interrupt();
            return null;
        }
    }
}
