package cz.projectsurvive.pslauncher;

import com.skcraft.launcher.Bootstrap;
import com.skcraft.launcher.bootstrap.BootstrapUtils;
import com.skcraft.launcher.bootstrap.SharedLocale;
import com.skcraft.launcher.bootstrap.SimpleLogFormatter;
import com.skcraft.launcher.bootstrap.SwingHelper;
import lombok.extern.java.Log;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Arrays;
import java.util.Locale;
import java.util.Properties;
import java.util.logging.Level;

// TODO: Use a reflection library
@Log
public class PSBootstrap extends Bootstrap
{
	public static final String   BOOTSTRAP_VERSION_FIELD = "BOOTSTRAP_VERSION";
	public static final Class<?> PROPERTIES_FILE_CLASS   = Bootstrap.class;
	public static final String   PROPERTIES_FILE_NAME    = "bootstrap.properties";
	public static final String   PROPERTY_VERSION        = "version";
	private final File     baseDir;
	private final boolean  portable;
	private final String[] originalArgs;

	public PSBootstrap(boolean portable, String[] args) throws IOException, NoSuchFieldException, IllegalAccessException
	{
		super(portable, args);

		Field baseDirField = Bootstrap.class.getDeclaredField("baseDir");
		baseDirField.setAccessible(true);
		baseDir = (File) baseDirField.get(this);
		this.portable = portable;
		originalArgs = args;
	}

	public static void main(String[] args) throws Throwable
	{
		overwriteVersion();

		// Copy of Bootstrap#main(String[]) follows...
		SimpleLogFormatter.configureGlobalLogger();
		SharedLocale.loadBundle("com.skcraft.launcher.lang.Bootstrap", Locale.getDefault());
		Method isPortableMode = Bootstrap.class.getDeclaredMethod("isPortableMode");
		isPortableMode.setAccessible(true);
		boolean portable = (boolean) isPortableMode.invoke(null);
		PSBootstrap bootstrap = new PSBootstrap(portable, args);

		bootstrap.startupProcedure();
    }

	public void startupProcedure() throws IOException
	{
		try
		{
			deleteBinariesIfNecessary();
			cleanup();
			launch();
		} catch (Throwable var4) {
			handleError(var4);
			System.exit(0);
		}
	}

	public void handleError(Throwable err) throws IOException
	{
		log.log(Level.WARNING, "Error", err);
		setSwingLookAndFeel();
		SwingHelper.showErrorDialog((Component) null, SharedLocale.tr("errors.bootstrapError"), SharedLocale.tr("errorTitle"), err);
		deleteBinariesNextTime();
	}

	public void deleteBinariesNextTime() throws IOException
	{
		System.out.println("Scheduling a deletion of the binaries for the next session.");
		new File(this.getBinariesDir(), ".delete").createNewFile();
	}

	public boolean shouldDeleteBinaries()
	{
		return new File(this.getBinariesDir(), ".delete").exists();
	}

	public void deleteBinariesIfNecessary() throws IOException
	{
		if(shouldDeleteBinaries())
		{
			System.out.println("Deleting binaries, as requested the previous session.");

			File[] binaries = this.getBinariesDir().listFiles();

			if(binaries != null)
				for(File file : binaries)
					deleteDirectory(file.toPath());
		}
	}

	public static void deleteDirectory(Path directory) throws IOException
	{
		Files.walkFileTree(directory, new SimpleFileVisitor<Path>()
		{
			@Override
			public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException
			{
				Files.delete(file);
				return FileVisitResult.CONTINUE;
			}

			@Override
			public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException
			{
				Files.delete(dir);
				return FileVisitResult.CONTINUE;
			}
		});
	}

	public static void overwriteVersion() throws NoSuchFieldException, IllegalAccessException, IOException, ClassNotFoundException
	{
		Field field = Bootstrap.class.getDeclaredField(BOOTSTRAP_VERSION_FIELD);
		Properties properties = BootstrapUtils.loadProperties(PROPERTIES_FILE_CLASS, PROPERTIES_FILE_NAME);
		int value = Integer.parseInt(properties.getProperty(PROPERTY_VERSION));

		Field modifiersField = Field.class.getDeclaredField("modifiers");

		modifiersField.setAccessible(true);
		modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);

		field.setAccessible(true);
		field.setInt(null, value);
	}

	public void execute(Class<?> clazz) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException
	{
		Method method = clazz.getDeclaredMethod("main", String[].class);
		String[] launcherArgs;

		/* YES! ACTUALLY USE THE VERSION FIELD. God. */
		Field versionField;

		try
		{
			versionField = Bootstrap.class.getDeclaredField(BOOTSTRAP_VERSION_FIELD);
		}
		catch(NoSuchFieldException e)
		{
			throw new RuntimeException(e);
		}

		versionField.setAccessible(true);
		String version = Integer.toString(versionField.getInt(null));

		if(this.portable) {
			launcherArgs = new String[]{"--portable", "--dir", this.baseDir.getAbsolutePath(), "--bootstrap-version", version};
		} else {
			launcherArgs = new String[]{"--dir", this.baseDir.getAbsolutePath(), "--bootstrap-version", version};
		}

		String[] args = new String[this.originalArgs.length + launcherArgs.length];
		System.arraycopy(launcherArgs, 0, args, 0, launcherArgs.length);
		System.arraycopy(this.originalArgs, 0, args, launcherArgs.length, this.originalArgs.length);
		log.info("Launching with arguments " + Arrays.toString(args));
		method.invoke(null, new Object[]{args});
	}
}
